package tin;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class FileService {

    private DataApi fileApi;
    private String user;
    private String password;
    private Scanner scanner;
    private Socket socket;

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {

        return path;
    }

    private String path;

    public FileService(DataApi fileApi, String user, String password, Socket socket, Scanner scanner) {
        this.fileApi = fileApi;
        this.user = user;
        this.password = password;
        this.socket = socket;
        this.scanner = scanner;
    }

    public String loggedMenu(String option) throws IOException {
        path = user;
        switch (option) {
            case ("LOGOUT"): {
                break;
            }
            case ("LS"): {
                fileApi.listFiles(user);
                while (true) {
                    System.out.println("AVAILABLE COMMANDS:\n\t-MENU\n\t\n\t-{folder}\n\t-MKDIR\n\t-DWL\n\t-RM\n\t-DWLABORT\n\t-UPL");
                    String goInto = scanner.nextLine();
                    if (goInto.equals("MENU"))
                        break;
                    switch (goInto) {
                        case "CD ..": {
                            path = path.substring(0, path.lastIndexOf("/"));
                            System.out.println("\n\tLocation: " + path);

                            fileApi.listFiles(path);
                            break;
                        }
                        case "MKDIR": {
                            System.out.println("\n\tEnter directory name:");
                            String dirName = scanner.nextLine();
                            fileApi.mkdir(path, dirName);
                            break;
                        }
                        case "DWL": {
                            System.out.println("\n\tLocation: " + path);
                            System.out.println("\n\tEnter file to download:");
                            String fileName = scanner.nextLine();
                            String priority = "0";
                            while (Integer.valueOf(priority) < 1 || Integer.valueOf(priority) > 10) {
                                System.out.println("\tSet priority(1-10):");
                                priority = scanner.nextLine();
                            }
                            download(path + "/" + fileName, priority);
                            break;
                        }
                        case "RM": {
                            System.out.println("\n\tLocation: " + path);
                            System.out.println("\n\tEnter file to remove:");
                            String file = scanner.nextLine();
                            fileApi.removeFile(path + "/" + file);
                            break;
                        }
                        case "LS": {
                            fileApi.listFiles(path);
                            break;
                        }
                        case "DWLABORT": {
                            fileApi.dwlAbort(path);
                            break;
                        }
                        case ("UPL"): {
                            System.out.println("\tEnter path to file:");
                            String pathToUploadedFile = scanner.nextLine();
                            Thread t = new Thread(new Uploader(path, pathToUploadedFile, new RequestBodyProvider(user, password), socket));
                            t.start();
                            break;
                        }
                        default: {
                            path = path + "/" + goInto;
                            System.out.println("\n\tLocation: " + path);
                            fileApi.listFiles(path);
                            break;
                        }
                    }
                }
            }
        }
        return option;
    }

    private void upl(String pathToUploadedFile) {
        Thread t = new Thread(new Uploader(path, pathToUploadedFile, new RequestBodyProvider(user, password), socket));
        t.start();
    }

    private void download(String path, String priority) throws IOException {
        DataOutputStream out = new DataOutputStream(socket.getOutputStream());
        String dwlRequest = new RequestBodyProvider(user, password).createDownloadFileBody(path, priority).toString() + '\0';
        out.writeBytes(dwlRequest);
    }
}
