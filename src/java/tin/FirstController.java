package tin;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.json.JSONException;

import java.io.IOException;
import java.net.Socket;

public class FirstController {
    private String userLogin;
    private String password;
    private Socket socket;

    public String getUserLogin() {
        return userLogin;
    }

    public String getPassword() {
        return password;
    }

    public Socket getSocket() {
        return socket;
    }

    @FXML
    private TextField user;
    @FXML
    private PasswordField passwordField;

    private FxSupport consoleApp;

    private Scene secondScene;

    public void setSecondScene(Scene scene) {
        secondScene = scene;
    }

    public void openSecondScene(ActionEvent actionEvent) {
        Stage primaryStage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(secondScene);
    }

    @FXML
    private void login(ActionEvent event) throws IOException, JSONException, InterruptedException {
        userLogin = user.getText();
        password = passwordField.getText();
        consoleApp = new FxSupport();
        socket = new Socket("168.63.56.27", 8888);
        if (consoleApp.menu("AUTH", userLogin, password, socket) == true)
            openSecondScene(event);
    }

}