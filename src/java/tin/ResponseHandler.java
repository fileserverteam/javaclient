package tin;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;

public class ResponseHandler implements Runnable {

    private Socket socket;
    private List<String> downloadedFiles;
    private HashMap<String, FileOutputStream> fileOutputStreamHashMap;
    private FileService fileService;
    private UserAuth userAuth;

    public ResponseHandler(UserAuth userAuth, FileService fileService, Socket socket) {
        this.userAuth = userAuth;
        this.fileService = fileService;
        this.socket = socket;
        this.downloadedFiles = new ArrayList<>();
        this.fileOutputStreamHashMap = new HashMap<>();
    }


    @Override
    public void run() {
        try {
            handleResponse();
        } catch (IOException | JSONException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void handleResponse() throws IOException, JSONException, InterruptedException {
        Thread.sleep(1000);
        Base64.Decoder decoder = Base64.getDecoder();

        while (true) {
            if (socket.getInputStream().available() > 0) {
                String json = RequestUtils.toString(socket.getInputStream());
                JSONObject response = (new JSONObject(json));
                if (authResponse(response)) {
                    handleAuthResponse(response);
                }
                if (rmResponse(response)) {
                    handleRmResponse(response);
                }

                if (lsResponse(response)) {
                    handleLsResponse(response);
                }

                if (response.getString("command").equals("DWL") && !response.getString("code").equals("200")) {
                    File downloadedFile = new File(response.getString("path"));
                    String fileName = downloadedFile.getName();
                    if (ifNewFile(fileName)) {
                        downloadedFiles.add(fileName);
                        fileOutputStreamHashMap.put(fileName, new FileOutputStream("C:\\Users\\Michał61\\Desktop\\DWL\\" + fileName, true));
                    }
                    String dataBase64 = response.getString("data");
                    byte[] data = decoder.decode(dataBase64);
                    fileOutputStreamHashMap.get(downloadedFile.getName()).write(data);
                }
            }
        }
    }

    private boolean authResponse(JSONObject response) throws JSONException {
        return response.getString("command").equals("AUTH");
    }

    private boolean ifNewFile(String file) {
        return !downloadedFiles.contains(file);
    }

    private void handleAuthResponse(JSONObject response) throws JSONException {
        if (response.getString("code").equals("401")) {
            userAuth.unauthorized();
        } else
            userAuth.authorized();
    }

    private boolean rmResponse(JSONObject response) throws JSONException {
        return response.getString("command").equals("RM");
    }

    private void handleRmResponse(JSONObject response) throws JSONException {
        if (response.getString("code").equals("409"))
            System.out.println("Path not found");
        else
            System.out.println("File removed");
    }

    private boolean lsResponse(JSONObject response) throws JSONException {
        return response.getString("command").equals("LS");
    }

    private void handleLsResponse(JSONObject response) throws JSONException {
        if (response.getString("code").equals("409")) {
            System.out.println("Path not found");
            String path = fileService.getPath();
            fileService.setPath(path.substring(0, path.lastIndexOf("/")));
        } else {
            DirectoryContentParser.showContent(response.toString());
        }
    }
}
