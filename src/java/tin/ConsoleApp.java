package tin;

import org.json.JSONException;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class ConsoleApp {
    private static Scanner scanner;
    private UserAuth userAuth;
    private static DataApi fileApi;
    private static String user;
    private static String password;
    private static Socket socket;
    private static FileService fileService;
    private static ResponseHandler responseHandler;
    private static Thread t;

    public static void main(String[] args) throws IOException, JSONException, InterruptedException {
        socket = new Socket("168.63.56.27", 8888);
        scanner = new Scanner(System.in);
        ConsoleApp consoleApp = new ConsoleApp();
        String option = "none";
        while (option != "EXIT") {
            unauthorizeHelp();
            option = scanner.nextLine();
            consoleApp.menu(option);
        }
    }

    public static void init(String command) throws IOException, JSONException, InterruptedException {
        socket = new Socket("168.63.56.27", 8888);
        ConsoleApp consoleApp = new ConsoleApp();
        while (command != "EXIT") {
            unauthorizeHelp();
            consoleApp.menu(command);
        }
    }


    private void readData() throws InterruptedException {
        responseHandler = new ResponseHandler(userAuth, fileService, socket);
        t = new Thread(responseHandler);
        t.start();
    }

    public void menu(String option) throws IOException, JSONException, InterruptedException {
        switch (option) {
            case ("HELP"): {
                unauthorizeHelp();
                break;
            }
            case ("EXIT"): {
                System.exit(200);
                break;
            }
            case ("AUTH"): {
                System.out.println("\t\nlogin:");
                user = scanner.nextLine();

                System.out.println("\t\npassword:");
                password = scanner.nextLine();
                if (userAuth == null)
                    userAuth = new UserAuth(user, password, socket);
                else {
                    userAuth.setLoggedIn(null);
                    userAuth.setUser(user);
                    userAuth.setPassword(password);
                }

                if (t == null) {
                    fileApi = new DataApi(new RequestBodyProvider(user, password), socket);
                    fileService = new FileService(fileApi, user, password, socket, scanner);
                    readData();
                }
                userAuth.login();
                while (isWaitingOnResponseFromServer()) {
                    System.out.println("Waiting for server response...");
                    Thread.sleep(300);
                }

                if (userAuth.isLoggedIn()) {
                    authorizeHelp();
                    fileService.loggedMenu(scanner.nextLine());
                    userAuth.unauthorized();
                } else {
                    System.out.println("Wrong credentials!");
                    break;
                }
            }
        }
    }


    private boolean isWaitingOnResponseFromServer() {
        return (userAuth.isLoggedIn() == null);
    }


    private static void unauthorizeHelp() {
        System.out.println("\nAvailable command:\n");
        System.out.println("\t-HELP");
        System.out.println("\t-AUTH");
        System.out.println("\t-EXIT\n\t");
    }

    private static void authorizeHelp() {
        System.out.println("\t\nONLY FOR AUTHORIZED USERS:");
        System.out.println("\t-LS");
        System.out.println("\t-LOGOUT\n\t");
    }
}
