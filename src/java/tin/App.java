package tin;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class App extends Application {
    @Override
    public void start(final Stage stage) throws Exception {

        FXMLLoader firstPaneLoader = new FXMLLoader(getClass().getResource("/login.fxml"));
        Parent firstPane = firstPaneLoader.load();
        Scene firstScene = new Scene(firstPane, 320, 179);

        FXMLLoader secondPageLoader = new FXMLLoader(getClass().getResource("/menu.fxml"));
        Parent secondPane = secondPageLoader.load();
        Scene secondScene = new Scene(secondPane, 600, 367);

        FirstController firstPaneController = firstPaneLoader.getController();
        firstPaneController.setSecondScene(secondScene);

        SecondController secondPaneController = secondPageLoader.getController();
        secondPaneController.setFirstController(firstPaneController);


        stage.setTitle("File server");
        stage.setScene(firstScene);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

