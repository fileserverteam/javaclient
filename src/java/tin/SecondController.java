package tin;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

public class SecondController {
    private Stage stage;
    private FirstController firstController;

    public void setFirstController(FirstController firstController) {
        this.firstController = firstController;
    }

    @FXML
    private TextArea area;

    @FXML
    private void uploadFile() throws JSONException, InterruptedException, IOException {
        File file = choseFile().get();
        String path = file.getAbsolutePath();

        Thread t = new Thread(new Uploader(path, path, new RequestBodyProvider(firstController.getUserLogin(), firstController.getPassword()), firstController.getSocket()));
        t.start();
    }

    private Optional<File> choseFile() {
        FileChooser fileChooser = new FileChooser();
        File selectedFile = fileChooser.showOpenDialog(stage);
        return Optional.ofNullable(selectedFile);
    }

    @FXML
    private void exit() {
        System.exit(200);
    }
}

