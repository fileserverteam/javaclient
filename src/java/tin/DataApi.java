package tin;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class DataApi {
    private RequestBodyProvider requestBodyProvider;
    private Socket socket;
    private DataOutputStream dataOutputStream;
    public DataApi(RequestBodyProvider requestBodyProvider, Socket socket) throws IOException {
        this.requestBodyProvider = requestBodyProvider;
        this.dataOutputStream = new DataOutputStream(socket.getOutputStream());
    }

    public void listFiles(String path) throws IOException {
        dataOutputStream.writeBytes(requestBodyProvider.createListFilesBody(path).toString() + '\0');
    }

    public void removeFile(String path) throws IOException {
        dataOutputStream.writeBytes(requestBodyProvider.createRemoveDataBody(path).toString() + '\0');
    }

    public void mkdir(String path, String fileName) throws IOException {
        dataOutputStream.writeBytes(requestBodyProvider.createUplMkdirFileBody("MKDIR", path, fileName).toString() + '\0');
    }

    public void dwlAbort(String path) throws IOException {
        dataOutputStream.writeBytes(requestBodyProvider.createDwlAbortBody(path).toString() + '\0');
    }
}


