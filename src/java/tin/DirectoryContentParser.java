package tin;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class DirectoryContentParser {

    public static void showContent(String jsonResponse) {
        JsonParser parser = new JsonParser();
        JsonObject directoryConetent = parser.parse(jsonResponse).getAsJsonObject();

        System.out.println("\t\nFiles:");
        showElements(directoryConetent, "files");

        System.out.println("\t\nDirectories:");
        showElements(directoryConetent, "dirs");
    }

    private static void showElements(JsonObject directoryContent, String type) {
        for (JsonElement content : directoryContent.getAsJsonArray(type)) {
            if (content != null)
                System.out.println("\t\t" + content.getAsString());
        }
    }
}
