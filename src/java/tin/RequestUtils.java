package tin;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public class RequestUtils {
    public static String toString(InputStream in) throws IOException {
        return new DataInputStream(in).readLine();
    }
}
