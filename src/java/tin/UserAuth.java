package tin;

import javafx.fxml.FXML;

import java.awt.*;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class UserAuth {

    private String user;
    private String password;
    private Boolean isLoggedIn;
    private Socket socket;
    private DataOutputStream out;
    private BufferedReader in;
    @FXML
    TextArea textArea;

    public void setUser(String user) {
        this.user = user;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLoggedIn(Boolean loggedIn) {
        isLoggedIn = loggedIn;
    }

    public UserAuth(String user, String password, Socket socket) {
        this.user = user;
        this.password = password;
        this.socket = socket;
    }

    public synchronized Boolean isLoggedIn() {
        return isLoggedIn;
    }

    public synchronized void login() throws IOException {
        RequestBodyProvider requestBodyProvider = new RequestBodyProvider(user, password);
        DataOutputStream out = new DataOutputStream(socket.getOutputStream());
        out.writeBytes(requestBodyProvider.createLoginBody().toString() + '\0');
    }

    public synchronized void unauthorized() {
        isLoggedIn = false;
    }

    public synchronized void authorized() {
        System.out.println("Successful authorization");
        isLoggedIn = true;
    }
}
