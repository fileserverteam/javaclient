package tin;

import org.json.JSONException;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class FxSupport {
    private static Scanner scanner;
    private UserAuth userAuth;
    private static DataApi fileApi;
    private static String user;
    private static String password;
    private static Socket socket;
    private static FileService fileService;
    private static ResponseHandler responseHandler;
    private static Thread t;

    public static void init(String command) throws IOException, JSONException, InterruptedException {

        ConsoleApp consoleApp = new ConsoleApp();
        while (command != "EXIT") {
            unauthorizeHelp();
            consoleApp.menu(command);
        }
    }


    public boolean menu(String option, String user, String password, Socket socket) throws IOException, JSONException, InterruptedException {
        switch (option) {
            case ("AUTH"): {
                if (userAuth == null)
                    userAuth = new UserAuth(user, password, socket);
                else {
                    userAuth.setLoggedIn(null);
                    userAuth.setUser(user);
                    userAuth.setPassword(password);
                }

                if (t == null) {
                    fileApi = new DataApi(new RequestBodyProvider(user, password), socket);
                    fileService = new FileService(fileApi, user, password, socket, scanner);
                    readData(socket);
                }
                userAuth.login();
                while (isWaitingOnResponseFromServer()) {
                    System.out.println("Waiting for server response...");
                    Thread.sleep(300);
                }

                if (userAuth.isLoggedIn()) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    private void readData(Socket socket) throws InterruptedException {
        responseHandler = new ResponseHandler(userAuth, fileService, socket);
        t = new Thread(responseHandler);
        t.start();
    }

    private boolean isWaitingOnResponseFromServer() {
        return (userAuth.isLoggedIn() == null);
    }


    private static void unauthorizeHelp() {
        System.out.println("\nAvailable command:\n");
        System.out.println("\t-HELP");
        System.out.println("\t-AUTH");
        System.out.println("\t-EXIT\n\t");
    }

    private static void authorizeHelp() {
        System.out.println("\t\nONLY FOR AUTHORIZED USERS:");
        System.out.println("\t-LS");
        System.out.println("\t-LOGOUT\n\t");
    }
}
