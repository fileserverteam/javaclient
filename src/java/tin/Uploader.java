package tin;

import com.google.gson.JsonObject;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONException;

import java.io.*;
import java.net.Socket;
import java.util.Arrays;

public class Uploader implements Runnable {

    private String pathToUploadedFile;
    private String serverPath;
    private RequestBodyProvider requestBodyProvider;
    private Socket socket;

    public Uploader(String serverPath, String pathToUploadedFile, RequestBodyProvider requestBodyProvider, Socket socket) {
        this.serverPath = serverPath;
        this.pathToUploadedFile = pathToUploadedFile;
        this.requestBodyProvider = requestBodyProvider;
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            uploadFile();
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    public void uploadFile() throws IOException, JSONException {
        File uploadedFile = new File(pathToUploadedFile);
        try (FileInputStream fileInputStream = new FileInputStream(uploadedFile);
             BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream)) {

            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            JsonObject uplRequest = requestBodyProvider.createUplMkdirFileBody("UPL", serverPath, uploadedFile.getName());
            int dataBytes = 0;
            byte[] data = new byte[1024];
            while ((dataBytes = bufferedInputStream.read(data, 0, 1024)) >= 0) {
                System.out.println(dataBytes);
                if (dataBytes < 1024) {
                    uplRequest.addProperty("data", new String(Base64.encode(Arrays.copyOfRange(data, 0, dataBytes)), "UTF-8"));
                } else {
                    uplRequest.addProperty("data", new String(Base64.encode(data)));
                }
                out.writeBytes(uplRequest.toString() + "\0");
            }
            uplRequest.addProperty("command", "UPLFIN");
            JsonObject uplFinRequest = requestBodyProvider.createUplMkdirFileBody("UPLFIN", serverPath, uploadedFile.getName());
            out.writeBytes(uplFinRequest.toString() + "\0");
        }
    }
}

