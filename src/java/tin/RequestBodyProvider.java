package tin;

import com.google.gson.JsonObject;

import java.io.IOException;

public class RequestBodyProvider {
    private String user;
    private String password;

    public RequestBodyProvider(String user, String password) {
        this.user = user;
        this.password = password;
    }

    public JsonObject createLoginBody() {
        JsonObject loginBody = basicBody("AUTH");
        loginBody.addProperty("username", user);
        loginBody.addProperty("password", password);
        return loginBody;
    }

    public JsonObject createListFilesBody(String path) {
        JsonObject listFileBody = basicBody("LS");
        listFileBody.addProperty("path", path);
        return listFileBody;
    }

    public JsonObject createUplMkdirFileBody(String command, String path, String fileName) throws IOException {
        JsonObject uplBody = basicBody(command);
        uplBody.addProperty("path", path);
        uplBody.addProperty("name", fileName);
        return uplBody;
    }

    public JsonObject createDownloadFileBody(String path, String priority) throws IOException {
        JsonObject basicBody = basicBody("DWL", path);
        basicBody.addProperty("priority", priority);
        return basicBody;
    }

    public JsonObject createRemoveDataBody(String path) throws IOException {
        return basicBody("RM", path);
    }

    public JsonObject createDwlAbortBody(String path) throws IOException {
        return basicBody("DWLABORT", path);
    }

    private JsonObject basicBody(String command, String path) throws IOException {
        JsonObject basicBodyFilePath = basicBody(command);
        basicBodyFilePath.addProperty("path", path);
        return basicBodyFilePath;
    }


    private JsonObject basicBody(String command) {
        JsonObject basicBody = new JsonObject();
        basicBody.addProperty("type", "REQUEST");
        basicBody.addProperty("command", command);
        return basicBody;
    }
}
